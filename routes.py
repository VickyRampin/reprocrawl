from flask import Flask, render_template, request, url_for
from elasticsearch import Elasticsearch
from flask_paginate import Pagination, get_page_parameter
import urllib.parse


app = Flask(__name__)

es = Elasticsearch()


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/resources')
def resources():
    query = request.args.get('query', '')
    tags = list_tags()
    active_tags = get_tags()
    page = request.args.get(get_page_parameter(), type=int, default=1)
    total, items = search(query, tags=get_tags(), from_=10 * (page - 1))
    pagination = Pagination(page=page, total=total, format_total=True, format_number=True, css_framework='bootstrap4', display_msg='''Displaying <b>{start} - {end}</b> {record_name} out of <b>{total}</b> total.''', alignment='center')
    return render_template(
        'resources.html',
        tags=tags, query=query, active_tags=active_tags,
        items=items, pagination=pagination)


def get_tags():
    """
    Gets the set of tags in the current query.
    """
    tags = request.args.get('tags')
    if tags:
        return set(tags.split('|'))
    return set()


def add_tag_link(tag):
    """
    Creates a link that adds a tag to the current query.
    """
    tags = get_tags()
    tags.add(tag)
    tags = '|'.join(sorted(tags))
    params = {'tags': tags}
    query = request.args.get('query')
    if query:
        params['query'] = query
    return '{0}?{1}'.format(
        url_for('resources'),
        urllib.parse.urlencode(params))


def remove_tag_link(tag):
    """
    Creates a link that removes a tag from the current query.
    """
    tags = get_tags()
    tags.discard(tag)
    tags = '|'.join(sorted(tags))
    params = {'tags': tags}
    query = request.args.get('query')
    if query:
        params['query'] = query
    return '{0}?{1}'.format(
        url_for('resources'),
        urllib.parse.urlencode(params))


@app.context_processor
def register_tag_links():
    """
    Registers the add_tag_link and remove_tag_link functions.
    """
    return dict(add_tag_link=add_tag_link,
                remove_tag_link=remove_tag_link)


def search(text, tags=None, limit=10, from_=0,
           ignore_tags=['Relevant', 'Neutral', 'Deep Crawl']):
    """
    Search Elasticsearch for pages matching the search text and tags.
    """
    # Construct the query
    # Note that we always add 'relevant' to the required tags
    query = [{'match': {'tag': "Relevant"}}]
    if tags:
        for tag in tags:
            query.append({'match': {'tag': tag}})
    if text:
        query.append({'match': {'_all': text}})

    # Query Elasticsearch
    body = {
        'query': {'bool': {'must': query}},
        '_source': ['title', 'url', 'tag'],
        'size': limit,
        'from': from_,
    }
    response = es.search(index='reproducibility', body=body)
    total = response['hits']['total']
    pages = [h['_source'] for h in response['hits']['hits']]

    # Ignore some tags
    ignore_tags = set(ignore_tags)
    for h in pages:
        h['tag'] = [t for t in h['tag'] if t not in ignore_tags]
    return total, pages


def list_tags():
    """
    Gets the list of all the tags from Elasticsearch.

    :return: A dictionary mapping tag name to number of pages under that tag.
    """
    body = {
        'aggs': {
            'alltags': {
                'terms': {
                    'field': 'tag',
                    'size': 0,
                },
            },
        },
        'size': 0,
    }
    response = es.search(index='reproducibility', body=body)
    buckets = response['aggregations']['alltags']['buckets']
    return {item['key']: item['doc_count'] for item in buckets}


if __name__ == '__main__':
    app.run(debug=True)
